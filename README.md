# Computational Finance


UPDATED : Cara menjalankan secara langsung
pertama kali setting di lokal adalah jalankan
- "stack install stack-run"
- "stack build"
- (Untuk windows "SET PORT=8080")
kemudian, setiap kali menjalankan runserver. lakukan "stack run"

(Alasan PORTnya gak dihardcode adalah karena di heroku maunya ngeassign sendiri dia)

untuk mengecek program, pada terminal lain
- curl -H "Content-Type: application/json" -d '{"unemploymentbenefit":0.3,"probabilityofgettingfired":0.4,"wagefrom":55,"leisure":0.5,"probabilityofgettinghired":0.4,"wageto":66,"weeklydiscountrate":0.2}' localhost:8080/calculate

or

- curl -H "Content-Type: application/json" -d '{"unemploymentbenefit":0.3,"probabilityofgettingfired":0.4,"wagefrom":55,"leisure":0.5,"probabilityofgettinghired":0.4,"wageto":66,"weeklydiscountrate":0.2}' http://kebab-api-haskell.herokuapp.com/calculate

## Back-End
### Implementasi
#### Main
File ini mengimplementasikan :
- Spock
- Memasukkan input-input sebagai Model dan wages
- Mengaplikasikan ddpsolve terhadap Model dan setiap wage

#### Job Search
File ini mengimplementasikan :
- Model dari Job Search yang terdiri dari Reward, Fungsi Transprob, Horizon, dan Discount
- Fungsi untuk membuat matrix Reward dan Fungsi Transprob
- Fungsi lainnya : Fungsi yang memasukkan seluruh elemen pada list menjadi elemen list dalam list, fungsi yang melakukan merge setiap list pada list dalam list, dan fungsi bantuan lainnya.

#### DDP Solve
File ini mengimplementasikan :
- Fungsi ddpsolve yang akan mengembalikan prediksi apakah employed atau unemployed lebih untung untuk aktif atau tidak aktif pada minggu selanjutnya
- Fungsi lainnya : Fungsi valmax yang mengembalikan index terbesar dalam baris matrix, fungsi valpol, dan fungsi bantuan lainnya.


