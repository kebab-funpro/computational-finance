{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE OverloadedStrings #-}

import           Web.Spock
import           Web.Spock.Config

import           Data.Aeson       hiding (json)
import           Data.Monoid      ((<>))
import           Data.Text        (Text, pack)
import           Data.List
import           Data.Number.Transfinite
import           GHC.Generics
import           JobSearch
import           DdpSolve
import           System.Environment

data Input = Input
    {unemploymentbenefit :: Float
    ,probabilityofgettinghired :: Float
    ,weeklydiscountrate :: Float
    ,leisure :: Float
    ,probabilityofgettingfired :: Float 
    ,wagefrom :: Float
    ,wageto :: Float
    }deriving(Generic, Show)

data Output = Output
    {wage :: Float
    ,unemployed :: Integer
    ,employed :: Integer
    }deriving(Generic, Show)

instance ToJSON Input

instance FromJSON Input

type API = SpockM () () () ()

type ApiAction a = SpockAction () () () a

main :: IO()
main = do { spockCfg <- defaultSpockCfg () PCNoDatabase ()
          ; port <- read <$> getEnv "PORT"
          ; runSpock port (spock spockCfg app) }

errorJson :: Int -> Text -> ApiAction ()
errorJson code message =
  json $
    object
    [ "result" .= String "failure"
    , "error" .= object ["code" .= code, "message" .= message]
    ]
    
corsHeader =
  do ctx <- getContext
     setHeader "Access-Control-Allow-Origin" "*"
     setHeader "Access-Control-Allow-Headers" "Content-Type"
     setHeader "Access-Control-Allow-Methods" "GET, POST, OPTIONS"
    
     pure ctx
app :: API
app = do
    prehook corsHeader $
        post "calculate" $ do
            maybeInput <- jsonBody :: ApiAction (Maybe Input)
            case maybeInput of
                Nothing -> errorJson 1 "Failed to parse request body as Input"
                Just theInput -> do {
                    --text $ pack (show (unemploymentbenefit theInput))
                    
                    -- assign model and wages with input
                    let model_ = Model { reward = getReward (unemploymentbenefit theInput) (leisure theInput) 55
                            , discount = (weeklydiscountrate theInput)
                            , transprob = getTransprob (probabilityofgettinghired theInput) (probabilityofgettingfired theInput)
                            , horizon = infinity
                            }
                        wages = getWages (wagefrom theInput) (wageto theInput)

                    -- results return list of output [Unemp, Emp] of ddpsolve for each wage
                        results = Data.List.map ddpsolve (Data.List.map (updateReward model_) (wages))
                        results2float = Data.List.map (Data.List.map fromIntegral) results

                    -- append each wage and result
                        table = zipList (list2lol wages) results2float
                        -- udah ditest : zipList (list2lol (getWages 55 65)) (Data.List.map ddpsolve (Data.List.map (updateReward model1) (getWages 55 65)))
                    in text $ pack (show table)

                    -- TO:DO : put table as Output data
                }