module DdpSolve where
import Data.List
import Data.Matrix
import Data.Number.Transfinite
import Data.Vector
import JobSearch

va = fromLists [[1],[1]]
fa = fromLists [[60,50],[60,55]]
pa = fromLists [[1,0], [1,0], [0.1,0.9], [0.1,0.9]]
deltaa = 0.99

test = valmax va fa pa deltaa

ddpsolve :: Model -> [Int]
ddpsolve m = do {
    let maxit = 100
        delta = (discount m)
        f = (reward m)
        p = (transprob m)
        n = nrows f
        v = fromLists [[1],[1]]
    in Data.Vector.toList (getCol 1 (valmax v f p delta))
}

-- DDP solve dengan bellman equation via Newton method
{- ddpsolve :: Model -> [Int]
ddpsolve m = do {
    let maxit = 100
        delta = (discount m)
        f = (reward m)
        p = (transprob m)
        n = nrows f
        vold = zero n 1
        xold = zero n 1
        
        --[v,x] = valmax(v,f,P,delta);          % update policy  
        --[pstar,fstar,ind] = valpol(x,f,P); 
        --v = (speye(n)-diagmult(delta(ind),pstar))\fstar;
    --in Data.Vector.toList (getRow 1 (valmax v f p delta))
    in printStringNTimes 0 = return ()
    printStringNTimes n = do {
        let x = valmax v f p delta
            if x == xold
                then
                    x
                else
                    do {
                        let vp = valpol x f p
                            dlt = fromLists [[delta],[delta],[delta],[delta]]
                            ind = [toIntegral (vp!!0)!!0, toIntegral (vp!!0)!!1]
                            pstar = vp!!2
                            fstar = vp!!1
                            v = (\) ((-) (identity n) (diagmult (Data.Vector.fromList (Data.List.map (getMatrixIndex dlt) ind)) pstar)) fstar
                            xold = x
                            vold = v
                        in printStringNTimes (n-1)
                    }
                    
     }

      printStringNTimes (n-1)

} -}

valmax :: Matrix Float -> Matrix Float -> Matrix Float -> Float -> Matrix Int
valmax v f p delta = do {
    let n = nrows f
        m = ncols f
    in  maxIndexRow ((+) f (reshape (Data.Vector.toList (getCol 1 (scaleMatrix delta (multStd p v))))))
    
}

valpol :: Matrix Int -> Matrix Float -> Matrix Float -> [[Float]]
valpol x f p = do {
    let n = nrows f
        ind = Data.Vector.toList (getCol 1 ((+) (scaleMatrix n x) (fromLists [[-1],[0]])))
        fstar = Data.List.map (getMatrixIndex f) ind
        pstar = Data.List.map (getMatrixIndex p) ind
        flind = [(fromIntegral (ind!!0)), (fromIntegral (ind!!1))]
    in [flind, fstar, pstar]
}

mat1 = fromLists [[(itf 2),(itf 6)],[(itf 10),(itf 1)]]

maxIndexRow :: Matrix Float -> Matrix Int
maxIndexRow a = fromLists [[(+) 1 (maxIndex (getRow 1 a))],[(+) 1 (maxIndex (getRow 2 a))]] 

reshape :: [Float] -> Matrix Float
reshape a =  fromLists ([[(a!!0), (a!!1)], [(a!!2), (a!!3)]])

getMatrixIndex :: Matrix Float -> Int -> Float
getMatrixIndex a n = (combinedColumn (ncols a) a)!!n

combinedColumn :: Int -> Matrix Float -> [Float]
combinedColumn 0 a = []
combinedColumn n a = (combinedColumn (n-1) a) Data.List.++ (Data.Vector.toList (getCol n a))

itf :: Float -> Float
itf n = n
