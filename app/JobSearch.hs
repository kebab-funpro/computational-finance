module JobSearch where
import Data.List
import Data.Matrix
import Data.Number.Transfinite
import Data.Vector

data Model 
    = Model 
          { 
              discount  :: Float,
              reward :: Matrix Float,
              transprob :: Matrix Float,
              horizon :: Float
          }
      deriving Show

getReward :: Float -> Float -> Float -> Matrix Float
getReward v u w = fromLists [[v,u],[v,w]]

getTransprob :: Float -> Float -> Matrix Float
getTransprob pfind pfire = fromLists [[1, 0], [1, 0], [1-pfind, pfind], [pfire, 1-pfire]]

getWages :: Float -> Float -> [Float]
getWages a b = [a..b]

-- change vector to 2x2 diagonal matrix
diagZero :: Vector Float -> Matrix Float
diagZero a = fromLists [[(Data.Vector.head a),0], [0,(Data.Vector.last a)]]

-- diagonal multiplication function 
diagmult :: Vector Float -> Matrix Float -> Matrix Float
diagmult a b = multStd (diagZero a) b

model1 = Model { reward = getReward 60 50 55
                , discount = 0.99
                , transprob = getTransprob 0.9 0.1
                , horizon = infinity
}

-- Function to update reward's wage
updateReward x w = x { reward = (setElem w (2,2) (reward x)) } 

-- Function to modify every element of list to element of list of list
list2lol [] = []
list2lol (x:xs) = [x] : list2lol xs 

-- Function to merge element of two 2D lists
zipList [] [] = []
zipList (x:xs) (y:ys) = (x Data.List.++ y) : (zipList xs ys) 

-- Tester
--main = do putStrLn "Wage start :"
--          wstart <- readLn
--          putStrLn "Wage end :"
--          wend <- readLn
--          print (Data.List.map ddpsolve [model1])
--          print (updateReward model1 44) --print isi model
